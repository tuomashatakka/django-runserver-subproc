# coding=utf-8
from __future__ import unicode_literals
from django.apps import apps
from django.core.servers.basehttp import get_internal_wsgi_application, run
from os import path
from subprocess import Popen
from django.conf import settings


def assets_server(*args):
    print(' -- Static assets')
    nodepath = path.join(settings.BASE_DIR, 'index.js')
    Popen(['node', nodepath])


def socket_server(*args):
    print(' -- SocketIO Express server')
    nodepath = path.join(settings.BASE_DIR, 'index.js')
    Popen(['node', nodepath])


def django_server(*args):

    def runserver(*args, **options):
        """Returns the default WSGI handler for the runner."""
        run('localhost', 8000, get_internal_wsgi_application(), threading=True)
        return get_internal_wsgi_application()

    try:
        print(' -- Django development server')
        runserver()
    except:
        return
