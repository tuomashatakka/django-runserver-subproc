import sys
from multiprocessing import Process, active_children, current_process
from django.core.management import BaseCommand
from logging import getLogger
from django.core.wsgi import get_wsgi_application
from django.apps import apps, registry, config
import django
from serve.management.server_initializers import (
    assets_server, django_server, socket_server)


log = getLogger(__name__).debug


def serve(proc, *args):
    print(current_process())
    try:
        proc(*args)
    except KeyboardInterrupt:
        pass


class Command(BaseCommand):
    help = "Allows running multiple daemons under one main process"

    # A command must define handle()
    def handle(self, *args, **options):
        self.check(display_num_errors=True)
        self.log("Starting servers...")

        # SERVERS_LIST = (
        #    ('Static assets', start_assets_server),
        #    ('Socket IO Express', start_socket_server),
        #    ('Django', start_django_server),
        # )
        # for (label, server) in SERVERS_LIST:
        #    print(label, server)
        #    self.start_process(server, label)

        try:
            self.persist()
        except KeyboardInterrupt:
            self.log("Termination signal triggered, shutting down")
            self.close_all()
        finally:
            self.log("All services up and running")

    def persist(self, *args, **options):
        for proc in [assets_server, django_server, socket_server]:
            Process(target=serve, args=[proc, ]).start()

    def log(self, *msg):
        message = msg[0].format(*msg[1:])
        self.stdout.write(message)

    def close_all(self):
        try:
            process_list = active_children()
            while len(process_list):
                proc = process_list[0]
                self.log("Terminating process {}", proc)
                proc.join()
        finally:
            self.log("All clear")
